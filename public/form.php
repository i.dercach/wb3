<!DOCTYPE html>
<html lang="ru">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Задание 3</title>
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
  </head>
  <body>
    
      <main>
        <div>
                <form action="" method="POST">
                    <h3 class="sign-in">Заполните данные</h3><br><br><br>
                
                
                
                    <label name="one"> <p>Имя</p> </label>
                  <input name="fio" value="Васька">
               
        
<br>

                <label>
                  E-mail:<br />
                  <input name="email"
                    value="email@example.com"
                    type="email" />
                </label><br />
        
                <label>
                  Дата рождения:<br />
                  <input name="birthday"
                    value="2000-01-01"
                    type="date" />
                </label><br />
        
                Пол:<br>

                <label><input type="radio" checked="checked"
                  name="gender" value="Male" >
                  Мужчина                  
                </label>

                <br>
                <label><input type="radio"
                  name="gender" value="Female" >
                  Женщина
                
                </label>
              
              
                <br>
                
                
                <label><input type="radio"
                  name="gender" value="Other"/>
                  Другой
                </label>
            
                

        <br>
                Количество конечностей:<br />
                <label><input type="radio"
                  name="limbs" value="0" />
                  0
                  <br>
                </label>
                <label><input type="radio"
                  name="limbs" value="1" />
                  1
                  <br>
                </label>
                <label><input type="radio"
                  name="limbs" value="2" />
                  2
                  <br>
                </label>
                <label><input type="radio"
                  name="limbs" value="4" />
                  4
                  <br>
                <label>
                  Сверхспособности:
                  <br />
                  <select name="ability[]"
                    multiple="multiple">
                    <option value="0">Летать</option>
                    <option value="1" selected="selected">Читать</option>
                    <option value="2" selected="selected">Писать</option>
                  </select>
                </label><br />
        
                <label>
                  Биография:<br />
                  <textarea name="biography">...</textarea>
                </label><br />
        
                <br />
                <label><input type="checkbox"
                  name="contract" />
                  С контрактом ознакомлен
                </label><br />
                
                <input id="submit" type="submit" value="ok" />
              </form>
          </div>
      </main>
      
  </body>

</html>